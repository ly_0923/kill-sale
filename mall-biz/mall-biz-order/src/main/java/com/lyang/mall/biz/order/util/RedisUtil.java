package com.lyang.mall.biz.order.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Transaction;
import redis.clients.jedis.Tuple;
import redis.clients.jedis.exceptions.JedisConnectionException;

/**   
 * @Title: RedisUtil.java 
 * @Package com.rd.p2p.core.core.util 
 * @Description: redis操作工具类 
 * @author liuyang  
 * @date 2017年7月12日 下午12:15:28 
 * @version V1.0   
*/
@SuppressWarnings("resource")
public class RedisUtil {
	
	//private static final Logger logger = Logger.getLogger(Logger.class);
	
	private static final String LOCK_SUCCESS = "OK";
    private static final String SET_IF_NOT_EXIST = "NX";
    private static final String SET_WITH_EXPIRE_TIME = "EX";

	
	/**
	 * Key前缀
	 */
	private static final String KEY_PREFIX = "";
	
	private static JedisPool jedisPool;
	
	static{
		jedisPool = (JedisPool)BeanUtil.getBean("jedisPool");
		System.out.println(jedisPool);
	}
	
	public static boolean exist(String key) {
        Jedis jedis = null;
        boolean ret = false;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.exists(key);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.exists(key);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static void del(String key) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.del(key);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                jedis.del(key);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
    }

    public static void expire(String key, int time) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.expire(key, time);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                jedis.expire(key, time);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
    }

    public static byte[] get(String key) {
        byte[] value = null;
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            value = jedis.get(key.getBytes());
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                value = jedis.get(key.getBytes());
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return value;
    }

    public static String getString(String key) {
        String value = null;
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            value = jedis.get(key);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                value = jedis.get(key);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return value;
    }

    public static String set(String key, byte[] value) {
        String ret = "";
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.set(key.getBytes(), value);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.set(key.getBytes(), value);
            }catch (JedisConnectionException e1) {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }
    
    /** 
     * 存储数据到缓存中，并制定过期时间和当Key存在时是否覆盖。 
     * @param key 
     * @param value 
     * @param nxxx nxxx的值只能取NX或者XX，如果取NX，则只有当key不存在时才进行set，如果取XX，则只有当key已经存在时才进行set 
     * @param expx expx的值只能取EX或者PX，代表数据过期时间的单位，EX代表秒，PX代表毫秒。 
     * @param time 过期时间，单位是expx所代表的单位。 
     * @return 
     */ 
    public static String set(String key, String value, String nxxx, String expx, long time) {
        String ret = "";
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.set(key, value, nxxx, expx, time);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.set(key, value, nxxx, expx, time);
            }catch (JedisConnectionException e1) {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static long setnx(String key, String value) {
        Jedis jedis = null;
        long ret = 0;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.setnx(key, value);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.setnx(key, value);
            }catch (JedisConnectionException e1) {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }


    public static String setString(String key, String value) {
        String ret="";
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.set(key,value);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.set(key,value);
            }catch (JedisConnectionException e1) {
            }
        } finally {
            returnJedis(jedis);
        }
        return  ret;
    }




    public static String setex(String key, int expire, byte[] value) {
        String ret = "";
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.setex(key.getBytes(), expire, value);
        }  catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.setex(key.getBytes(), expire, value);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }


    public static String setStringEx(String key, int expire, String value) {
        String ret = "";
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.setex(key, expire, value);
        }  catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.setex(key, expire, value);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static Long incr(String key) {
        Jedis jedis = null;
        Long ret = (long)0;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.incr(key);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.incr(key);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static Long incrBy(String key, long by) {
        Jedis jedis = null;
        Long ret = (long)0;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.incrBy(key,by);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.incrBy(key,by);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static Map<String, byte[]> mgetByteArr(String[] keys) {
        byte[][] b = new byte[keys.length][];
        for (int i=0;i<b.length;i++){
            b[i]=keys[i].getBytes();
        }
        Jedis jedis = null;
        Map<String, byte[]> ret = new HashMap<String, byte[]>();
        try {
            jedis = jedisPool.getResource();
            List<byte[]> values = jedis.mget(b);
            for( int i=0; i<keys.length; i++ ) {
                if(values.get(i)!=null){
                    ret.put(keys[i], values.get(i));
                }
            }
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                List<byte[]> values = jedis.mget(b);
                for( int i=0; i<keys.length; i++ ) {
                    if(values.get(i)!=null){
                        ret.put(keys[i], values.get(i));
                    }
                }
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static Map<String, String> mgetToMap(String... keys) {
        Jedis jedis = null;
        Map<String, String> ret = new HashMap<String, String>();
        try {
            jedis = jedisPool.getResource();
            List<String> values = jedis.mget(keys);
            for (int i = 0; i < keys.length; i++) {
                if (values.get(i) != null) {
                    ret.put(keys[i], values.get(i));
                }
            }
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                List<String> values = jedis.mget(keys);
                for (int i = 0; i < keys.length; i++) {
                    if (values.get(i) != null) {
                        ret.put(keys[i], values.get(i));
                    }
                }
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static List<String> mget(String... key) {
        Jedis jedis = null;
        List<String> value = null;

        try {
            jedis = jedisPool.getResource();
            value = jedis.mget(key);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                value = jedis.mget(key);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return value;
    }


    public static void rpushMulti(String key, List<byte[]> values) {
        Jedis jedis = null;
        String[] strValues = new String[values.size()];

        for( int i=0; i<values.size(); i++ ) {
            strValues[i] = new String(values.get(i));
        }
        try {
            jedis = jedisPool.getResource();
            jedis.rpush(key, strValues);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                jedis.rpush(key, strValues);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
    }

    public static void rpushMultiInString(String key, List<String> values) {
        Jedis jedis = null;
        String[] strValues = new String[values.size()];

        for( int i=0; i<values.size(); i++ ) {
            strValues[i] = values.get(i);
        }
        try {
            jedis = jedisPool.getResource();
            jedis.rpush(key, strValues);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                jedis.rpush(key, strValues);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
    }


    public static void lpushMulti(String key, List<byte[]> values) {
        Jedis jedis = null;
        String[] strValues = new String[values.size()];

        for( int i=0; i<values.size(); i++ ) {
            strValues[i] = new String(values.get(i));
        }
        try {
            jedis = jedisPool.getResource();
            jedis.lpush(key, strValues);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                jedis.lpush(key, strValues);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
    }

    public static void lpushMultiInString(String key, List<String> values) {
        Jedis jedis = null;
        String[] strValues = new String[values.size()];
        for( int i=0; i<values.size(); i++ ) {
            strValues[i] = values.get(i);
        }
        try {
            jedis = jedisPool.getResource();
            jedis.lpush(key, strValues);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                jedis.lpush(key, strValues);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
    }

    public static List<String> lrange(String key, int offset ,int limit) {
        Jedis jedis = null;
        List<String> ret = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.lrange(key, offset, limit);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.lrange(key, offset, limit);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static List<byte[]> lrangereturnbyte(String key, int offset ,int limit) {
        Jedis jedis = null;
        List<byte[]> ret = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.lrange(key.getBytes(), offset, limit);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.lrange(key.getBytes(), offset, limit);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static String hget(String key, String field) {
        Jedis jedis = null;
        String value = null;

        try {
            jedis = jedisPool.getResource();
            value = jedis.hget(key, field);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                value = jedis.hget(key, field);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return value;
    }

    public static Map<String, String> hgetall(String key) {
        Jedis jedis = null;
        Map<String, String> value = null;
        try {
            jedis = jedisPool.getResource();
            value = jedis.hgetAll(key);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                value = jedis.hgetAll(key);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return value;
    }

    public static Map<String, String> hmgetToMap(String key, String... fields) {
        Jedis jedis = null;
        Map<String, String> ret = new HashMap<String, String>();
        if (fields != null && fields.length != 0) {
            try {
                jedis = jedisPool.getResource();
                List<String> values = jedis.hmget(key, fields);
                for (int i = 0; i < fields.length; i++) {
                    if (values.get(i) != null) {
                        ret.put(fields[i], values.get(i));
                    }
                }
            } catch (JedisConnectionException e) {
                try {
                    jedis = jedisPool.getResource();
                    List<String> values = jedis.hmget(key, fields);
                    for (int i = 0; i < fields.length; i++) {
                        if (values.get(i) != null) {
                            ret.put(fields[i], values.get(i));
                        }
                    }
                } finally {
                }
            }finally {
                returnJedis(jedis);
            }
        }
        return ret;
    }

    public static long hset(String key, String field, String value) {
        Jedis jedis = null;
        long ret = 0;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.hset(key, field, value);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.hset(key, field, value);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }


    public static String hmset(String key, Map<String, String> hash) {
        Jedis jedis = null;
        String ret = "";
        try {
            jedis = jedisPool.getResource();
            ret = jedis.hmset(key, hash);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            ret = jedis.hmset(key, hash);
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static long hlen(String key) {
        Jedis jedis = null;
        long value = 0L;
        try {
            jedis = jedisPool.getResource();
            value = jedis.hlen(key);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            value = jedis.hlen(key);
        }finally {
            returnJedis(jedis);
        }
        return value;
    }

    public static Boolean hexists(String key, String field) {
        Jedis jedis = null;
        Boolean value = null;
        try {
            jedis = jedisPool.getResource();
            value = jedis.hexists(key, field);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            value = jedis.hexists(key, field);
        }finally {
            returnJedis(jedis);
        }
        return value;
    }

    public static double zincrby(String key, double addValue, String member) {
        Jedis jedis = null;
        double newscore;
        try {
            jedis = jedisPool.getResource();
            newscore = jedis.zincrby(key,addValue,member);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            newscore = jedis.zincrby(key,addValue,member);
        }finally {
            returnJedis(jedis);
        }
        return newscore;
    }
    public static void hrem(String key, String field) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.hdel(key, field);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                jedis.hdel(key, field);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
    }

    public static Set<String> hkeys(String key) {
        Jedis jedis = null;
        Set<String> ret = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.hkeys(key);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.hkeys(key);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static long lpush(String key,String...vaule) {
        Jedis jedis = null;
        long ret = 0L;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.lpush(key,vaule);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.lpush(key,vaule);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static long lrem(String key,int count,String vaule) {
        Jedis jedis = null;
        long ret = 0L;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.lrem(key,count,vaule);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.lrem(key,count,vaule);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static String lpop(String key) {
        Jedis jedis = null;
        String ret = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.lpop(key);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.lpop(key);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static long rpush(String key,String... vaule) {
        Jedis jedis = null;
        long ret = 0L;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.rpush(key,vaule);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.rpush(key,vaule);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }
    
    public static String rpop(String key) {
        Jedis jedis = null;
        String ret = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.rpop(key);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.rpop(key);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }
    
    public static String lindex(String key,long index) {
        Jedis jedis = null;
        String ret = "";
        try {
            jedis = jedisPool.getResource();
            ret = jedis.lindex(key,index);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.lindex(key,index);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static long hdel(String key,String... files) {
        Jedis jedis = null;
        long ret = 0L;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.hdel(key,files);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.hdel(key,files);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static long hincrBy(final String key, final String field, final long value) {
        Jedis jedis = null;
        long ret = 0L;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.hincrBy(key, field, value);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.hincrBy(key, field, value);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static void zadd(String key, long score, String value) {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            jedis.zadd(key, score, value);
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                jedis.zadd(key, score, value);
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
    }

    public static long zrem(String key, String value) {
        Jedis jedis = null;
        long ret = 0;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.zrem(key.getBytes(), value.getBytes());
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.zrem(key.getBytes(), value.getBytes());
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }



    public static long zremMulti(String key, String[] values) {
        Jedis jedis = null;
        long ret = 0;
        if( values == null || values.length == 0 )
            return 0;

        try {
            jedis = jedisPool.getResource();
            ret = jedis.zrem(key, values);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.zrem(key, values);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static Double zscore(String key, String field) {
        Jedis jedis = null;
        Double ret = (Double)0.0;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.zscore(key.getBytes(), field.getBytes());
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.zscore(key.getBytes(), field.getBytes());
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }
	
	public static Set<String> zrangeByScore(String key, long start, long end) {
        Jedis jedis = null;
        Set<byte[]> byteRet = null;
        Set<String> ret = new HashSet<String>();

        try {
            jedis = jedisPool.getResource();
            byteRet = jedis.zrangeByScore(key.getBytes(), start, end);
            if( byteRet != null ) {
                for( byte[] it: byteRet ) {
                    ret.add( it.toString() );
                }
            }
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                byteRet = jedis.zrangeByScore(key.getBytes(), start, end);
                if( byteRet != null ) {
                    for( byte[] it: byteRet ) {
                        ret.add( it.toString() );
                    }
                }
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static Set<byte[]> zrevrange(String key, long start, long end) {
        Jedis jedis = null;
        Set<byte[]> ret = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.zrevrange(key.getBytes(), start, end);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.zrevrange(key.getBytes(), start, end);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static Set<byte[]> zrange(String key, long start, long end) {
        Jedis jedis = null;
        Set<byte[]> ret = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.zrange(key.getBytes(), start, end);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.zrange(key.getBytes(), start, end);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static Set<String> zrevrangeRetString(String key, long start, long end) {
        Jedis jedis = null;
        Set<String> ret = null;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.zrevrange(key, start, end);
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.zrevrange(key, start, end);
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static Map<String, Double> zrevrangeWithScores(String key, long start, long end) {
        Jedis jedis = null;
        Map<String, Double> ret = new HashMap<String, Double>();
        try {
            jedis = jedisPool.getResource();
            Set<Tuple> tuples = jedis.zrevrangeWithScores(key, start, end);
            for (Tuple tuple : tuples) {
                ret.put(tuple.getElement(), tuple.getScore());
            }
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                Set<Tuple> tuples = jedis.zrevrangeWithScores(key, start, end);
                for (Tuple tuple : tuples) {
                    ret.put(tuple.getElement(), tuple.getScore());
                }
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static Map<String, Double> zrangeWithScores(String key, long start, long end) {
        Jedis jedis = null;
        Map<String, Double> ret = new HashMap<String, Double>();
        try {
            jedis = jedisPool.getResource();
            Set<Tuple> tuples = jedis.zrangeWithScores(key, start, end);
            for (Tuple tuple : tuples) {
                ret.put(tuple.getElement(), tuple.getScore());
            }
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                Set<Tuple> tuples = jedis.zrangeWithScores(key, start, end);
                for (Tuple tuple : tuples) {
                    ret.put(tuple.getElement(), tuple.getScore());
                }
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static List<String> zrevrangeByScoreString(String key, long start, long end, int offset, int limit) {
        Jedis jedis = null;
        Set<byte[]> byteRet = null;
        List<String> ret = new ArrayList<String>();

        try {
            jedis = jedisPool.getResource();
            byteRet = jedis.zrevrangeByScore(key.getBytes(), start, end, offset, limit);
            if( byteRet != null ) {
                for( byte[] it: byteRet ) {
                    ret.add(new String(it));
                }
            }
        }catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                byteRet = jedis.zrevrangeByScore(key.getBytes(), start, end, offset, limit);
                if( byteRet != null ) {
                    for( byte[] it: byteRet ) {
                        ret.add(new String(it));
                    }
                }
            } finally {
            }
        } finally {
            returnJedis(jedis);
        }
        return ret;
    }
	
	 public static  List<Long> zrevrangeByScoreLong(String key, long start, long end, int offset, int limit) {
	        Jedis jedis = null;
	        Set<byte[]> byteRet = null;
	        List<Long> ret = new ArrayList<Long>();

	        try {
	            jedis = jedisPool.getResource();
	            byteRet = jedis.zrevrangeByScore(key.getBytes(), start, end, offset, limit);
	            if( byteRet != null ) {
	                for( byte[] it: byteRet ) {
	                    ret.add(Long.parseLong(new String(it)));
	                }
	            }
	        }catch (JedisConnectionException e) {
	            try {
	                jedis = jedisPool.getResource();
	                byteRet = jedis.zrevrangeByScore(key.getBytes(), start, end, offset, limit);
	                if( byteRet != null ) {
	                    for( byte[] it: byteRet ) {
	                        ret.add(Long.parseLong(new String(it)));
	                    }
	                }
	            } finally {
	            }
	        } finally {
	            returnJedis(jedis);
	        }
	        return ret;
	    }



	    public static long lLen(String key){
	        Jedis jedis = null;
	        long ret = 0;
	        try {
	            jedis = jedisPool.getResource();
	            ret = jedis.llen(key);
	        } catch (JedisConnectionException e) {
	            try {
	                jedis = jedisPool.getResource();
	                ret = jedis.llen(key);
	            } finally {
	            }
	        }finally {
	            returnJedis(jedis);
	        }
	        return ret;
	    }

	    public static Long zcard(String key) {
	        Jedis jedis = null;
	        Long ret = (long)0;
	        try {
	            jedis = jedisPool.getResource();
	            ret = jedis.zcard(key.getBytes());
	        } catch (JedisConnectionException e) {
	            try {
	                jedis = jedisPool.getResource();
	                ret = jedis.zcard(key.getBytes());
	            } finally {
	            }
	        }finally {
	            returnJedis(jedis);
	        }
	        return ret;
	    }

	    public static long zcount(String key, long min, long max) {
	        Jedis jedis = null;
	        long ret = 0;
	        try {
	            jedis = jedisPool.getResource();
	            ret = jedis.zcount(key, min, max);
	        }catch (JedisConnectionException e) {
	            try {
	                jedis = jedisPool.getResource();
	                ret = jedis.zcount(key, min, max);
	            } finally {
	            }
	        } finally {
	            returnJedis(jedis);
	        }
	        return ret;
	    }

	    public static void sadd(String key, String value) {
	        Jedis jedis = null;
	        try {
	            jedis = jedisPool.getResource();
	            jedis.sadd(key, value);
	        } catch (JedisConnectionException e) {
	            try {
	                jedis = jedisPool.getResource();
	                jedis.sadd(key, value);
	            } finally {
	            }
	        }finally {
	            returnJedis(jedis);
	        }
	    }

	    public static long scard(String key) {
	        Jedis jedis = null;
	        long ret = 0;
	        try {
	            jedis = jedisPool.getResource();
	            ret = jedis.scard(key);
	        } catch (JedisConnectionException e) {
	            try {
	                jedis = jedisPool.getResource();
	                ret = jedis.scard(key);
	            } finally {
	            }
	        }finally {
	            returnJedis(jedis);
	        }
	        return ret;
	    }
	
    public static boolean sismember(String key, String field) {
        Jedis jedis = null;
        boolean ret = false;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.sismember(key.getBytes(), field.getBytes());
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                ret = jedis.sismember(key.getBytes(), field.getBytes());
            } finally {
            }
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }

    public static String srandmember(String key) {
        Jedis jedis = null;
        String result;
        try {
            jedis = jedisPool.getResource();
            result = jedis.srandmember(key);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            result = jedis.srandmember(key);
        }finally {
            returnJedis(jedis);
        }
        return result;
    }

    public static Set<String> smembers(String key) {
        Jedis jedis = null;
        Set<String> result;
        try {
            jedis = jedisPool.getResource();
            result = jedis.smembers(key);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            result = jedis.smembers(key);
        }finally {
            returnJedis(jedis);
        }
        return result;
    }

    public static Long srem(String key, String... members) {
        Jedis jedis = null;
        Long result;
        try {
            jedis = jedisPool.getResource();
            result = jedis.srem(key, members);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            result = jedis.srem(key, members);
        }finally {
            returnJedis(jedis);
        }
        return result;
    }


    public static Set<String> keys(String pattern) {
        Jedis jedis = null;
        Set<String> ret;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.keys(pattern);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            ret = jedis.keys(pattern);
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }
	
	public static Long renamenx(String oldkey, String newkey) {
        Jedis jedis = null;
        Long ret;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.renamenx(oldkey, newkey);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            ret = jedis.renamenx(oldkey, newkey);
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }
	
   public static Long ttl(String key) {
        Jedis jedis = null;
        Long ret;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.ttl(key);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            ret = jedis.ttl(key);
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }
	
	public static String watch(String... keys) {
        Jedis jedis = null;
        String ret;
        try {
            jedis = jedisPool.getResource();
            ret = jedis.watch(keys);
        } catch (JedisConnectionException e) {
            jedis = jedisPool.getResource();
            ret = jedis.watch(keys);
        }finally {
            returnJedis(jedis);
        }
        return ret;
    }
	
	/**
    * 获取资源
	* @return
	*/
	public static Jedis getJedis(){
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
            return jedis;
        } catch (JedisConnectionException e) {
            try {
                jedis = jedisPool.getResource();
                return jedis;
            } catch (JedisConnectionException e1) {
                try {
                    jedis = jedisPool.getResource();
                    return jedis;
                } finally {
                }
            }
        }
	}
	
	private static String setPrefix(String key) {
	    key=KEY_PREFIX+"_"+key;
	    return key;
	}
	
	private static void returnJedis(Jedis jedis) {
        if(jedis != null){
        	jedis.close();
        }
    }
	
	public static void close() {
		jedisPool.close();
    }
	
	public static void closePipeline(Pipeline pipeline) {
        if (pipeline != null) {
            try {
                pipeline.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}
	
    public static void closeTransaction(Transaction transaction) {
        if (transaction != null) {
            try {
                transaction.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
	
    public static int getNumActive() {
        return jedisPool.getNumActive();
    }
    
    /**
     * 尝试获取分布式锁
     * @param key 关键字
     * @param value 值
     * @param expireTime 超期时间 默认为秒
     * @return 是否获取成功
     */
    public static boolean setIfNotExist(String key, String value, int expireTime) {
        String result = set(key, value, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME, expireTime);
        if (LOCK_SUCCESS.equals(result)) {
            return true;
        }
        return false;

    }

    /**
     * 尝试获取分布式锁
     * @param key 关键字
     * @param expireTime 超期时间 默认为秒
     * @return 是否获取成功
     */
	public static boolean tryLock(String key, int expireTime) {
		return setIfNotExist(key,"1",expireTime);
	}

	
}
