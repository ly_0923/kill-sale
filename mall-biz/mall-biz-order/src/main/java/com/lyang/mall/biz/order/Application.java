package com.lyang.mall.biz.order;

import com.lyang.mall.biz.order.util.BeanUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@ImportResource({"classpath:dubbo-provider.xml","classpath:druid-bean.xml"})
@ServletComponentScan     //扫描filter和servlet
public class Application
{
    public static void main( String[] args )
    {
       SpringApplication.run(Application.class);
    }
}
