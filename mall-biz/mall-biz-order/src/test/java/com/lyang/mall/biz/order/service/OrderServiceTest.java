package com.lyang.mall.biz.order.service;

import com.lyang.mall.api.order.entity.StockOrder;
import com.lyang.mall.api.order.service.OrderService;
import com.lyang.mall.biz.order.Application;
import com.lyang.mall.biz.order.mapper.StockOrderMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class OrderServiceTest {
    @Resource
    private OrderService orderService;

    @Resource
    private StockOrderMapper StockOrderMapper;

    @Test
    public void testSelectById(){
        StockOrder stockOrder = StockOrderMapper.selectByPrimaryKey(1);
        System.out.println(stockOrder.getName());
    }

}