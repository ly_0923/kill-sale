package com.lyang.mall.common.entity;

import java.io.Serializable;

/**
 * @Title: ResultObj.java
 * @Description: 响应信息
 * @author ly
 * @date 2018年3月10日 上午11:18:08
 * @version V1.0
 */
public class ResultObj implements Serializable {

    private static final long serialVersionUID = -2517109528658469939L;

    public static final int succCode = 200;
    public static final int failCode = 500;
    private int code;

    private String msg;

    private Object data;

    public ResultObj(){}

    public ResultObj(int code,String msg){
        this.code=code;
        this.msg=msg;
    }

    public static ResultObj getSuccess(){
        return new ResultObj(succCode,"成功!");
    }

    public static ResultObj getFail(){
        return new ResultObj(failCode,"失败!");
    }

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public Object getData() {
        return data;
    }
    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "【响应信息】code="+code+",msg="+msg;
    }
}
