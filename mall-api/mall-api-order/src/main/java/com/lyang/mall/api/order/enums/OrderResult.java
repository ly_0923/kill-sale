package com.lyang.mall.api.order.enums;

public enum OrderResult {
    SUCCESS(200,"操作成功!"),
    FAIL(300,"操作失败!"),
    ERROR(500,"系统异常"),
    STOCK_NOT_EXIST(1001,"库存不存在！"),
    STOCK_EMPTY(1002,"库存不足!"),
    UPDATE_STOCK_FAIL(1003,"更新库存失败!"),
    ;

    private int code;
    private String msg;

    private OrderResult(int code,String msg){
        this.code= code;
        this.msg= msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "返回响应信息，code="+code+",msg="+msg;
    }
}
