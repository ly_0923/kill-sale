package com.lyang.mall.api.order.service;

import com.lyang.mall.api.order.entity.Stock;
import com.lyang.mall.common.entity.ResultObj;

public interface OrderService {

    /**
     *  下单
     * @param stockId 仓库id
     * @return
     */
    public ResultObj createOrder(Integer  stockId);

    /**
     *  创建Stock实体类并存入数据库
     * @param stock
     */
    public void createStockOrder(Stock stock);

}
