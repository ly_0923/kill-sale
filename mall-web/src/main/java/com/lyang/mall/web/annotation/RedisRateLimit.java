package com.lyang.mall.web.annotation;

import java.lang.annotation.*;

/**
 * 限流注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RedisRateLimit {
    int limit() default 5;
    int timeout() default 1000;
}
