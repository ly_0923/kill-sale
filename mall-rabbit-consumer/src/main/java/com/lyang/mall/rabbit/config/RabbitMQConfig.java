package com.lyang.mall.rabbit.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    public static final String QUEUE_NAME_ORDER = "order_queue";
    public static final String EXCHANGE_NAME_ORDER = "order_exchange";
    public static final String ROUNTING_KEY_ORDER = "order_rounting_key";

    /**
     * 创建队列
     * @return Queue
     */
    @Bean
    Queue queue(){
        return new Queue(QUEUE_NAME_ORDER,true);
    }

    /**
     * 创建交换机
     * @return TopicExchange
     */
    @Bean
    TopicExchange exchange(){
        return new TopicExchange(EXCHANGE_NAME_ORDER);
    }

    /**
     * 交换机和队列通过rountingKey绑定起来
     * @param queue
     * @param TopicExchange   这里是TopicExchange
     * @return
     */
    @Bean
    Binding binding(Queue queue, TopicExchange exchange){
        return  BindingBuilder.bind(queue).to(exchange).with(ROUNTING_KEY_ORDER);
    }
}
